FROM ubuntu
RUN apt-get update \
    && apt-get install -y ssh git git-ftp \
    && rm -rf /var/lib/apt/lists/*

COPY .ssh/ /root/.ssh

RUN chmod 600 /root/.ssh/*