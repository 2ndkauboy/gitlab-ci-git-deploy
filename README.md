# Installation

- Create a SSH key pair and out it into the `.ssh` Folder.
- Build the Docker image with the keys bundled in the image.
- You could also mount them as volumes from the deploy job.